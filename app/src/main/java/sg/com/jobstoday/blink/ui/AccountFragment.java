package sg.com.jobstoday.blink.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import sg.com.jobstoday.blink.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class AccountFragment extends Fragment {

    private static final int PROFILE = 0x0;
    private static final int SALARY = 0x1;
    private static final int PAYSLIP = 0x2;
    private static final int OTHERS = 0x3;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    // Views
    private RelativeLayout profileView;
    private RelativeLayout salaryView;
    private RelativeLayout payslipView;
    private RelativeLayout othersView;

    private Button btnProfile;
    private Button btnSalary;
    private Button btnPayslip;
    private Button btnOthers;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AccountFragment newInstance(String param1, String param2) {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_account, container, false);

        btnPayslip = (Button) view.findViewById(R.id.btnPayslip);
        btnPayslip.setOnClickListener(new AccountTabClickListener());
        btnProfile = (Button) view.findViewById(R.id.btnProfile);
        btnProfile.setOnClickListener(new AccountTabClickListener());
        btnSalary = (Button) view.findViewById(R.id.btnSalary);
        btnSalary.setOnClickListener(new AccountTabClickListener());
        btnOthers = (Button) view.findViewById(R.id.btnOthers);
        btnOthers.setOnClickListener(new AccountTabClickListener());

        // Set default view - Profile View
        selectView(PROFILE);


        return view;
    }

    /**
     *
     * @param view
     */
    private void selectView(int view) {
        // Reset Button selection
        btnProfile.setSelected(false);
        btnPayslip.setSelected(false);
        btnSalary.setSelected(false);
        btnOthers.setSelected(false);

        switch (view) {
            case PROFILE:
                btnProfile.setSelected(true);

                break;
            case SALARY:
                btnSalary.setSelected(true);

                break;
            case PAYSLIP:
                btnPayslip.setSelected(true);

                break;
            case OTHERS:
                btnOthers.setSelected(true);

                break;
        }
    }

    private class AccountTabClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch(v.getId()) {
                case R.id.btnPayslip:
                    selectView(PAYSLIP);

                    break;
                case R.id.btnProfile:
                    selectView(PROFILE);

                    break;
                case R.id.btnSalary:
                    selectView(SALARY);

                    break;
                case R.id.btnOthers:
                    selectView(OTHERS);

                    break;
            }

        }
    }


}
