package sg.com.jobstoday.blink.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import sg.com.jobstoday.blink.R;


/**
 * @author  Yunarta
 */
public class StyledTextView extends TextView implements Styled
{
    public StyledTextView(Context context)
    {
        this(context, null);
    }

    public StyledTextView(Context context, AttributeSet attrs)
    {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    public StyledTextView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        setTypeface(context, attrs);
    }

    private void setTypeface(Context context, AttributeSet attrs)
    {
        Typeface typeface = getTypeface();

        int style = 0;
        if (typeface != null)
        {
            style = typeface.getStyle();
        }

        String fontFile = null;
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.StyledTextView);
        if (ta != null)
        {
            int n = ta.getIndexCount();

            for (int i = 0; i < n; i++)
            {
                int attr = ta.getIndex(i);
                switch (attr)
                {
                    case R.styleable.StyledTextView_font:
                    {
                        fontFile = ta.getString(attr);
                        break;
                    }

                    case R.styleable.StyledTextView_html:
                    {
                        String html = ta.getString(attr);
                        setText(Html.fromHtml(html, new ImageGetter(), null));
                        break;
                    }
                }
            }

            if (!isInEditMode())
            {
                if (!TextUtils.isEmpty(fontFile))
                {
                    if (_font.get(fontFile) == null)
                    {
                        _font.put(fontFile, Typeface.createFromAsset(getResources().getAssets(), "fonts/" + fontFile));
                    }

                    typeface = _font.get(fontFile);
                    if (typeface != null)
                    {
                        setTypeface(typeface, style);
                    }
                }
            }
        }
    }

    private class ImageGetter implements Html.ImageGetter
    {

        public Drawable getDrawable(String source)
        {
            if (isInEditMode())
            {
                return null;
            }

            int id = getResources().getIdentifier(source, "drawable", getContext().getPackageName());
//            if (id != 0)
            {
                Drawable d = getResources().getDrawable(id);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());

                return d;
            }
//            else
//            {
//                return null;
//            }
        }
    }

}
