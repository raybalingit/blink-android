package sg.com.jobstoday.blink.views;

import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Yunarta
 */
public interface Styled
{
    Map<String, Typeface> _font = new HashMap<String, Typeface>();
}
