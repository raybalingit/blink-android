package sg.com.jobstoday.blink.mi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import java.util.HashMap;
import java.util.Map;

import sg.com.jobstoday.blink.R;

/**
 * Created by yunarta on 15/5/14.
 */
public abstract class BaseFragmentActivity extends FragmentActivity
{
    protected Map<Integer, FragmentTrackInfo> mTrackInfoMap;

    protected Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mHandler = new Handler();
        mTrackInfoMap = new HashMap<Integer, FragmentTrackInfo>();
    }

    public void startFragment(Intent intent)
    {

    }

    public void onDialogResult(int code, Intent intent)
    {

    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if (isFinishing())
        {
            overridePendingTransition(R.anim.anim_fade_from_left, R.anim.anim_fade_to_right);
        }
    }

    public boolean mayContinue() {
        return !isFinishing();
    }

    /**
     * Called by Fragment.startActivityForResult() to implement its behavior.
     */
    public void startActivityFromFragment(Fragment fragment, Intent intent, int requestCode)
    {
        if (fragment instanceof WaitingForResult)
        {
            FragmentManager fm = fragment.getFragmentManager();
            int id = fm.getFragments().indexOf(fragment);

            FragmentTrackInfo info = new FragmentTrackInfo(id);

            Fragment parent = fragment;
            while ((parent = parent.getParentFragment()) != null)
            {
                fm = parent.getFragmentManager();
                id = fm.getFragments().indexOf(parent);
                info = new FragmentTrackInfo(id, info);
            }

            mTrackInfoMap.put(requestCode, info);
        }

        super.startActivityForResult(intent, requestCode);
    }

    /**
     * Dispatch incoming result to the correct fragment.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        FragmentTrackInfo trackInfo = mTrackInfoMap.remove(requestCode & 0xffff);
        if (trackInfo != null)
        {
            FragmentManager fm = getSupportFragmentManager();
            Fragment fragment = fm.getFragments().get(trackInfo.mId);

            if (trackInfo.mChild != null)
            {
                FragmentTrackInfo childInfo = trackInfo;
                Fragment child = fragment;

                while ((childInfo = childInfo.mChild) != null) {
                    fm = child.getChildFragmentManager();
                    child = fm.getFragments().get(childInfo.mId);
                }

                fragment = child;
            }

            fragment.onActivityResult(requestCode, resultCode, data);
            return;
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
//
//
//        List<Fragment> fragments = getSupportFragmentManager().getFragments();
//
//        mFragments.noteStateNotSaved();
//        int index = requestCode >> 16;
//        if (index != 0)
//        {
//            index--;
//            if (mFragments.mActive == null || index < 0 || index >= mFragments.mActive.size())
//            {
//                Log.w(TAG, "Activity result fragment index out of range: 0x"
//                        + Integer.toHexString(requestCode));
//                return;
//            }
//            Fragment frag = mFragments.mActive.get(index);
//            if (frag == null)
//            {
//                Log.w(TAG, "Activity result no fragment exists for index: 0x"
//                        + Integer.toHexString(requestCode));
//            }
//            else
//            {
//                frag.onActivityResult(requestCode & 0xffff, resultCode, data);
//            }
//            return;
//        }
//
//        super.onActivityResult(requestCode, resultCode, data);
    }

    public class FragmentTrackInfo
    {

        public FragmentTrackInfo mChild;

        public int mId;

        public FragmentTrackInfo(int id)
        {
            mId = id;
        }

        public FragmentTrackInfo(int id, FragmentTrackInfo info)
        {
            mId = id;
            mChild = info;
        }

        public int getId()
        {
            int requestId = 0;
            if (mChild != null)
            {
                requestId |= mChild.getId() << 8;
            }

            requestId |= mId;
            return requestId;
        }
    }
}
