package sg.com.jobstoday.blink.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import sg.com.jobstoday.blink.R;

public class MainActivity extends ActionBarActivity {
    public static final String PROJECT = "project";
    public static final String TODAY = "today";
    public static final String ACCOUNT = "account";

    private Button mBtnProject;
    private Button mBtnToday;
    private Button mBtnAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set custom layout for action bar
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_blink);

        // Set up the Bottom Tab Bars
        mBtnProject = (Button) findViewById(R.id.btnTabProjects);
        mBtnProject.setOnClickListener(new TabClickListener());
        mBtnToday = (Button) findViewById(R.id.btnTabToday);
        mBtnToday.setOnClickListener(new TabClickListener());
        mBtnAccount = (Button) findViewById(R.id.btnTabAccount);
        mBtnAccount.setOnClickListener(new TabClickListener());

        // Set Initial Fragment (ProjectFragment)
        ProjectFragment projFrag = ProjectFragment.newInstance("", "");
        setFragment(projFrag, PROJECT);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Sets the username shown on the Action Bar
     *
     * @param username string username of the logged in user
     */
    public void setUsername(String username) {
        View actionBarView = getSupportActionBar().getCustomView();
        TextView tvTitle = (TextView) actionBarView.findViewById(R.id.tvUsername);
        tvTitle.setText(username);
    }

    /**
     *
     */
    private class TabClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnTabAccount:
                    AccountFragment acctFrag = AccountFragment.newInstance("", "");
                    setFragment(acctFrag, ACCOUNT);
                    break;
                case R.id.btnTabProjects:
                    ProjectFragment projFrag = ProjectFragment.newInstance("", "");
                    setFragment(projFrag, PROJECT);
                    break;
                case R.id.btnTabToday:
                    TodayFragment tdFrag = TodayFragment.newInstance("", "");
                    setFragment(tdFrag, TODAY);
                    break;
            }
        }
    }

    /**
     *
     * @param fragment
     * @param tag
     */
    private void setFragment(Fragment fragment, String tag) {
        // Pop Backstacks if present, to prevent back button issues
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }

        // Get the Fragment Transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Set Animation to be used
        ft.setCustomAnimations(R.anim.anim_fade_from_left, R.anim.anim_fade_from_right);
        ft.replace(R.id.frame, fragment, tag);
        ft.commit();
    }
}
